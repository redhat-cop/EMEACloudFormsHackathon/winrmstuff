C:
mkdir C:\temp
cd C:\temp

# set the prox conf (see: https://martin.hoppenheit.info/blog/2015/set-windows-proxy-with-powershell/)

$reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
$settings = Get-ItemProperty -Path $reg
# $settings.ProxyServer
# $settings.ProxyEnable
# Set-ItemProperty -Path $reg -Name ProxyServer -Value "proxy.example.org:8080"
Set-ItemProperty -Path $reg -Name ProxyServer -Value "de%5Ccdd8fe:SDDCrh2018!@rb-proxy-de.bosch.com:8080"
Set-ItemProperty -Path $reg -Name ProxyEnable -Value 1

curl http://rb-mirror.de.bosch.com/SDDC_POC/Certificates/BOSCH-CA-DE.cer -outfile C:\temp\BOSCH-CA-DE.cer
certutil.exe -addstore Root C:\temp\BOSCH-CA-DE.cer
timeout 20 /NOBREAK
# del C:\temp\BOSCH-CA-DE.cer

# curl http://rb-mirror.de.bosch.com/SDDC_POC/Certificates/BOSCH-CA1-DE.cer -outfile C:\temp\BOSCH-CA1-DE.cer
# certutil.exe -addstore Root C:\temp\BOSCH-CA1-DE.cer
# timeout 20 /NOBREAK
# del C:\temp\BOSCH-CA1-DE.cer

curl http://rb-mirror.de.bosch.com/SDDC_POC/Altiris_agent/AeXNSCHTTPs.exe -outfile C:\temp\AeXNSCHTTPs.exe
C:\temp\AeXNSCHTTPs.exe
timeout 20 /NOBREAK
# del C:\temp\AeXNSCHTTPs.exe

curl http://rb-mirror.de.bosch.com/SDDC_POC/Altiris_agent/SoftwareManagementSolution_Plugin_x64.msi -outfile C:\temp\SoftwareManagementSolution_Plugin_x64.msi
msiexec C:\temp\SoftwareManagementSolution_Plugin_x64.msi /qn /norestart
timeout 20 /NOBREAK
# del C:\temp\SoftwareManagementSolution_Plugin_x64.msi

curl http://rb-mirror.de.bosch.com/SDDC_POC/Altiris_agent/Symantec_InventoryAgent_x64.msi -outfile C:\temp\Symantec_InventoryAgent_x64.msi
msiexec C:\temp\Symantec_InventoryAgent_x64.msi /qn /norestart
timeout 20 /NOBREAK
# del C:\temp\Symantec_InventoryAgent_x64.msi

curl http://rb-mirror.de.bosch.com/SDDC_POC/Altiris_agent/Symantec_ServerInventoryAgent_x64.msi -outfile C:\temp\Symantec_ServerInventoryAgent_x64.msi
msiexec C:\temp\Symantec_ServerInventoryAgent_x64.msi /qn /norestart
timeout 30 /NOBREAK
# del C:\temp\Symantec_ServerInventoryAgent_x64.msi

# Here we need feedback from Bosch on why they have a Win64.msi file, and in their own env do use a x64.msi file.
# The Win64.msi file doesn't work, as we don't know the Product ID...
# And the MSIEXEC fails...
# Due to the last task never finish, this will reboot the Windows but the Agent is installed
shutdown /r /t 120


curl http://rb-mirror.de.bosch.com/SDDC_POC/Altiris_agent/Altiris_PatchMgmtAgent_Win64.msi -outfile C:\temp\Altiris_PatchMgmtAgent_Win64.msi
msiexec C:\temp\Altiris_PatchMgmtAgent_Win64.msi /qn /norestart
timeout 60 /NOBREAK
# del C:\temp\Altiris_PatchMgmtAgent_Win64.msi

## Run patches
## C:\Program Files\Altiris\Altiris Agent\Agents\PatchMgmtAgent\AeXPatchUtil.exe /Xa

