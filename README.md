
PROJECT SCOPE:   

Allow AD user management on Microsoft Active Directory Domain controller directly from CloudForms UI.   

USE CASE SCENARIO:   
*  CloudForms is main interface for administrators and users of different tenants.   
*  Tenant administrators and users are authenticating to CloudForms with Microsoft Active Directory   
*  Active Directory has one forest with dedicated OU for each tenant.   
*  Each OU has two security groups  - admins and users.   
*  Tenant administrators has no direct access to Active Directory console and they need to manage their users: list, create, delete, modify user, set and change it's password.   
*  Password need to be compliant with password complexity policy.   
*  Tenant users need possibility to change their password.   

FUTURE:   
*  Improvement of user experience by adding dashboard with users account state   


LINKS:   
*  Git - https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/winrmstuff.git   
*  Project - https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/introduction/issues/22
    

WORKFLOW DIAGRAM:   

![winrm](/uploads/fd887e365cdbb38b178867443fa2cb21/winrm.jpeg)   
   
---
   
ENVIROMENT:   

* Workstation      
* CloudForms   
* Ansible Tower   
* vCenter   
* ESXi   
* Microsoft AD Controller   
* GitLab repository   

PREREQUISITION:  
   
Windows:   
* Deploy Windows Server 2008 R2 from template available on vCenter
* Setup static IP address   
* Setup and configure Active Directory Domain controller role   
* Create OU's *tenant01* and *tenant02*   

![image](/uploads/444a75eba7f40477327a781a6a870266/image.png)   

* Setup and configure DNS service role   
* Setup and configure WinRM on AD controller to allow communication with AD (*PowerShell 3.0 required*)   
   
[ansible.ps1](/uploads/882be13eb35410ba1dcb98f03644a9ae/ansible.ps1)


ANSIBLE TOWER CONFIGURATION:   
   
*  Install required packages on Ansible Tower to allow communication with AD   

``yum -y install python-pip
pip install "pywinrm>=0.3.0"``   
   
*  Create new project and define proper GitLab repository   
*  Create new Inventory with manually added AD controller host   
   
![image](/uploads/b5d7f537875d724914540ee52d00a2ae/image.png)    
   
*  Create new vault credentials - this is the place where password will be temporary stored securely   

![image](/uploads/d34704e3e450b568ff1e826f41098ed9/image.png)   
   
*  Create new Job Template with Prompt on launch checked   
   
![image](/uploads/e0a426dc4175fb20b855bcec537dbc14/image.png)   
   
   
CLOUDFORMS CONFIGURATION   
   
*  Add AD Domain as authentication provider   
   
![image](/uploads/5dd4c1fc3ea27e27bed1f023954878a4/image.png)
   
*  Create new tenants to reflect tenat structure on AD   
   
![image](/uploads/a8534e052ec42ff07165f4aa7d3fa29b/image.png)   
   
*  Add Ansible Tower as provider   
   
![image](/uploads/1815fabf3ee6896d20c3af78b06fc28e/image.png)   

METHODS  
   
*  Within domain create Namespace, Class, Dialog with "CheckPasswordStrength" Instance - *this method is used to check password complexity using cracklib package*.
   
![image](/uploads/0951ca35221fbf79cfd2db637be3baad/image.png)
   
*  Create "CheckPasswordStrength" Method with code:   
   
```ruby
$evm.log("info","****************************************************** ComparePasswords Method Started ******************************************************")
password = $evm.root.decrypt('dialog_param_adpassword') 
value = `echo #{password} | cracklib-check | cut -d: -f2`
list_values = {
  'required'   => true,
  'protected'   => false,
  'read_only'  => true,
  'value' => value,
}
list_values.each { |key, value| $evm.object[key] = value }
```
   
   
*  Create Instance and Method called "ComparePasswords" - *it allow to check if provided password in two separate fields are the same*   
   
```ruby
$evm.log("info","****************************************************** ComparePasswords Method Started ******************************************************")

value = "No"
if $evm.root.decrypt('dialog_param_adpassword') == $evm.root.decrypt('dialog_param_adpassword2')
  value = "Yes"
end
list_values = {
  'required'   => true,
  'protected'   => false,
  'read_only'  => true,
  'value' => value,
}
list_values.each { |key, value| $evm.object[key] = value } 
```   
   
*  Create Instance and Method called "GetTenant" - *allows to retrieve current tenant and pass it as variable.*   
   
```ruby

$evm.log("info","****************************************************** GetTenant Method Started ******************************************************")

value = $evm.root['user'].current_tenant

$evm.log("info","#{value}")

list_values = {
  'required'   => true,
  'protected'   => false,
  'read_only'  => true,
  'value' => value,
}
list_values.each { |key, value| $evm.object[key] = value }
```   
   
*  Create Instance and Method called "ListADUsers" -*method allow to list all users AD within tenant's OU
  
```ruby
$evm.log("info","****************************************************** ListADUsers Method Started ******************************************************")
  
  tenant = $evm.root['tenant'].name
  existing_users = {}
  existing_users['!'] = '-- Exsisting Users Lookup --'
  list = `ldapsearch -x -h WIN-SI2KK3M1IHU.hackaton.cfme -D "Administrator@hackaton.cfme" -w ******** -b "ou=#{tenant},dc=hackaton,dc=cfme" | grep userPrincipalName | sed 's/userPrincipalName: //'`
  list.each_line do |line|
    existing_users["#{line}"] = "#{line.split("@").first}"
  end

  list_values = {
     'sort_by'    => :value,
     'data_type'  => :string,
     'required'   => true,
     'values'     => existing_users
  }
  list_values.each { |key, value| $evm.object[key] = value }
```  

  
SERVICE DIALOG  
   
* Add a Service Dialog  
  
  
![image](/uploads/9bc74778c3ddc1bfb32a91c9fc4e8735/image.png)  
  

*  Add a Field *"Exsisting Users (reference only)"* with *"ListADUsers"* method as entry point  
   
![image](/uploads/67b977335f7f3ba44d27a005976ad1a3/image.png)  

  
![2018-06-21_17h54_34](/uploads/1acdbdb855cd1b4b654f3815c36ab0d5/2018-06-21_17h54_34.png)   
![2018-06-21_18h04_47](/uploads/d9fba46df65b3d37fa3b4a108c205e74/2018-06-21_18h04_47.png)   
![2018-06-21_18h05_15](/uploads/b5f4fa491ca4ad265ff0a28e300e6203/2018-06-21_18h05_15.png)   

   
*  Add a Field *"Login Name* with *"param_adlogin"* value as a Name  
  
  
  ![image](/uploads/ec5f518877667fcb0c112d8172f1c0f2/image.png)  
  
![image](/uploads/2766498c467635f9e16338e19b2af530/image.png)  
  
![image](/uploads/34689ff0ec867a3b94e396e857acb782/image.png)  
  
  
*  Add a Field *"First Name"* with *"param_adfirstname"* value as Name
  
![image](/uploads/70f7a1fef2a3d2124df89330c1d1922c/image.png)  
  
  
![image](/uploads/2f54d14814ddf842651d6373bb443f96/image.png)  

  
*  Add a Field *"Last Name"* with *"param_adsurname"* value as Name  
  
  ![image](/uploads/9f3e0f35118b3755b1d919be1eea6378/image.png)  
  
![image](/uploads/220c37ee0959acbc49fb709326caaea8/image.png)  
  
![image](/uploads/16325587afa6d6b1164efe31cc5d2f6b/image.png)  

*  Add a Field *"Select Action To Be Taken"* with *"param_aduserstate"* value as Name  
  
![image](/uploads/c15362264e3a79e7c5ab5682f96c03e8/image.png)  
  
![image](/uploads/2bfbce43a624b78e90154489d277df27/image.png)  
  
![image](/uploads/4bed4f6885d32e47acc0819f2c4dd6c3/image.png)  
  
*  Add a Field *"Enabled"* with *"param_adenabled"* value as Name  

![image](/uploads/21adefdbcb44f879302d8e2e517906d4/image.png)  
  
![image](/uploads/7ced62b621899f8146a118d7868fca7b/image.png)  
  
![image](/uploads/b5a8f54235240e9009590512c3192f7b/image.png)  

*  Add a Field *"Select User Type"* with *"param_adgroup"* value as Name  
  
![image](/uploads/f66cadd9a4b3b1acc1338a1cdf62460f/image.png)  
  
![image](/uploads/0e1eecef37b85528b413e80c98bc51e0/image.png)  
  
![image](/uploads/920ea5d497cdec27f362ac4bf2c4aab5/image.png)  

*  Add a Field *"Password"* with *"param_adpassword"* value as a Name  
  
![image](/uploads/cd0d3c75866cb4d5bf4eaa32de2f1e28/image.png)
  
![image](/uploads/8370cf5e9d3ebb178373cd38ddaa2e44/image.png)  

![image](/uploads/d3c29a40503d1b2a73a65ac31287a5b5/image.png)  
  
*  Add a Field *"Confirm password"* with *"param_adpassword2"* value as a Name  

![image](/uploads/cec381c488001013a2482aeb303d011c/image.png)  
  

![image](/uploads/188c5247c9e1c54dc4ea3fbdba738ed4/image.png)  
  
*  Add a Field *"Password Match"* with *"passwords_match"* value as a Name and *"ComparePasswords"* method as an entry point  
  
![image](/uploads/123916b58fa3cd631032cec4945b0468/image.png)  

![image](/uploads/b1e517101d0b4ffd91c3eb18f0fd9fed/image.png)  
  ![image](/uploads/6733d96814de599ba58eeb5d0630f7e5/image.png)  

  ![image](/uploads/d04011c01482c40d1404bf8bcebd97b1/image.png)  

*  Add a Field *"Password Strenght"* with *"password_strength"* value as a Name and *"password_strength"* method as an entry point  

  ![image](/uploads/7500ac3265c828b1d4dbd0b04d79076c/image.png)  
![image](/uploads/6d5c68a3bea824d5f57e68a424433442/image.png)  
![image](/uploads/e7e594dc806de0104caf49f598aef71c/image.png)  
![image](/uploads/f24cc86047e0c783609985cfe0f6373c/image.png)  

*  Add a Field *"Tenant"* with *"param_adtenant"* value as a Name and *"GetTenant"* method as an entry point  
  
![image](/uploads/b38802b9f9542b8c5ad2febc0538d0d9/image.png)  
![image](/uploads/855cde3e88c107268584c734aa294aa3/image.png)  
![image](/uploads/281c8c889399d14675444e2384e78eda/image.png)  
![image](/uploads/2b0794f1661127aa5b4c2dd5419d4f83/image.png)  

*  Add a new catalog item  
  
![image](/uploads/620cd247b7221d56c10f50d783b9623a/image.png)

  
